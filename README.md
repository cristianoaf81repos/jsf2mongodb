# Sistema de Cadastro de Livros em JSF, Hibernate-ogm e MongoDB

Sistema de cadastro desenvolvido para demonstração (projeto maven)

## Instalação

Para instalação faz-se necessário o uso do apache tomcat 8.5 ou 9.0  
IDE recomendada para compilar: Eclipse ide JavaEE 2020-03.

## Ferramentas
[TOMCAT] (https://tomcat.apache.org/download-90.cgi)    
[ECLIPSE] (https://www.eclipse.org/downloads/packages/release/2020-03/m1)  
[MONGODB] (https://www.mongodb.com/download-center/community)  
obs: a versão do mongodb utilizada no projeto foi a 4.2.6  

## Uso
Editar o arquivo hibernate.ogm.config.xml, localizado no diretório
src/main/resorces e alterar as linhas representadas abaixo:  

```xml
<property name="hibernate.ogm.datastore.username">user_name_here</property>		
<property name="hibernate.ogm.datastore.password">password_here</property>
```

onde ->user_name_here<- , insira o nome do seu usuário do mongodb.  
onde ->password_here<-, insira a senha do seu usuário do mongodb.  


## Contribuições
O sistema tem apenas a finalidade de aprendizado e demonstração, mas sinta-se à vontade
para alterá-lo de acordo com seus interesses e necessidades.  

Por favor certifique-se de atualizar o projeto para baixar as dependências definidas  
no arquivo pom.xml.  

## Licença
[GPL3](https://choosealicense.com/licenses/gpl-3.0/)
