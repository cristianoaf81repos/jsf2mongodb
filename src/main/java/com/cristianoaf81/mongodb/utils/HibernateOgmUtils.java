package com.cristianoaf81.mongodb.utils;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.ogm.OgmSessionFactory;
import org.hibernate.ogm.boot.OgmSessionFactoryBuilder;

public class HibernateOgmUtils {
	
	private static OgmSessionFactory SF = createOgmSessionFactory();
	
	public static Logger LOGGER =
			Logger.getLogger(HibernateOgmUtils.class.getName());
	
	private static OgmSessionFactory createOgmSessionFactory() {
		try {
			
			StandardServiceRegistry reg = new StandardServiceRegistryBuilder()
					.configure("hibernate.ogm.config.xml")
					.build();
			
			
			return new MetadataSources(reg)
					.buildMetadata()
					.getSessionFactoryBuilder()
					.unwrap(OgmSessionFactoryBuilder.class)
					.build();
			
		} catch (HibernateException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
			throw new ExceptionInInitializerError(e);
		}
		//return null;
	}
	
	public static OgmSessionFactory getOgmSessionFactory() {
		return SF;
	}	

}
