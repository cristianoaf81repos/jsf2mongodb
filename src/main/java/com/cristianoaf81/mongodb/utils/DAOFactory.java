package com.cristianoaf81.mongodb.utils;

import com.cristianoaf81.mongodb.model.BookDAO;
import com.cristianoaf81.mongodb.model.BookDAOImpl;

public class DAOFactory {
	
	public static BookDAO createBookDAO() {
		BookDAOImpl bookDAOImpl = new BookDAOImpl();
		
		bookDAOImpl.setSession(HibernateOgmUtils
				.getOgmSessionFactory().openSession());
		
		return bookDAOImpl;
	}
	
}
