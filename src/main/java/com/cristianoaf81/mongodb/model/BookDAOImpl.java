package com.cristianoaf81.mongodb.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.ogm.OgmSession;

import com.cristianoaf81.mongodb.utils.HibernateOgmUtils;

public class BookDAOImpl implements BookDAO {
	
	private static final Logger LOGGER = Logger
			.getLogger(BookDAOImpl.class.getName());
	
	private OgmSession session;
	
	
	
	public OgmSession getSession() {
		return session;
	}



	public void setSession(OgmSession session) {
		this.session = session;
	}



	@Override
	public boolean saveBookData(Book b) {
		
		boolean returnValue = false;
		
		try {
			grantSessionOpen(); // garante a session aberta para transação			
			session.beginTransaction();
			if(b.getId().equals("") || b.getId() == null 
					|| b.getId().isBlank() || b.getId().isEmpty())
				session.save(b);
			else {
				session.update(b);
				session.flush();//é necessário chamar o flush para de fato atualizar
			}		
			
			session.getTransaction().commit();
			session.close();
			returnValue = true;
			
		}catch(HibernateException error){
			
			LOGGER.log(Level.SEVERE, error.getMessage(), error);
			
		}finally {
			
			session.close();
			
		}
		
		return returnValue;
	}



	@Override
	public List<Book> loadBooks() {
		List<Book> returnValue = new ArrayList<>();
		try {
			grantSessionOpen();
			
			String jpql = "from Book";
			
			session.beginTransaction();
			
			returnValue = session.createQuery(jpql, Book.class)
					.getResultList();
			
			session.close();
			
		} catch (HibernateException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
		}
		
		return returnValue;
	}
	
	public void grantSessionOpen() {
		// tenta abrir a conexao caso algum problema ocorra
		if(!session.isOpen())
			
			session = HibernateOgmUtils
			.getOgmSessionFactory().openSession();

	}

	@Override
	public boolean deleteBook(Book b) {
		boolean returnValue = false;
		
		try {
			grantSessionOpen(); // garante a session aberta para transação			
			session.beginTransaction();
			session.remove(b);
			session.flush();//é necessário chamar o flush para de fato atualizar			
			session.getTransaction().commit();
			session.close();
			returnValue = true;
			
		}catch(HibernateException error){
			
			LOGGER.log(Level.SEVERE, error.getMessage(), error);
			
		}finally {
			
			session.close();
			
		}
		
		return returnValue;
	}



	
}
