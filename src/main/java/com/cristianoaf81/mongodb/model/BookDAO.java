package com.cristianoaf81.mongodb.model;

import java.util.List;

public interface BookDAO {
	boolean saveBookData(Book b);
	List<Book> loadBooks();
	boolean deleteBook(Book b);
	
}
