package com.cristianoaf81.mongodb.business;


import com.cristianoaf81.mongodb.utils.DAOFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;

import com.cristianoaf81.mongodb.business.exceptions
.BookDataVerificationException;

import com.cristianoaf81.mongodb.model.Book;
import com.cristianoaf81.mongodb.model.BookDAO;


public class BookBO {
	
	private BookDAO BookDAO;
	private BookVerificationServiceImpl bookVerificationService;
	private static Logger LOGGER = Logger.getLogger(BookBO.class.getName()); 
	
	public BookBO() {
		this.BookDAO = DAOFactory.createBookDAO();
		this.bookVerificationService = new BookVerificationServiceImpl();
	}
	
	public boolean saveBook(Book b) {
		
		boolean returnValue = false;
		
		try {
			
			boolean isBookDataOk = 
					this.bookVerificationService.verifyBookData(b);
			
			if(isBookDataOk) {
				if(this.BookDAO != null) {
					
					returnValue = this.BookDAO.saveBookData(b);
					
				}else {
					
					this.BookDAO = DAOFactory.createBookDAO();
					
					returnValue = this.BookDAO.saveBookData(b);
					
				}
					
			}
			
		} catch (BookDataVerificationException dve) {
			
			LOGGER.log(Level.SEVERE, dve.getMessage(), dve);
			
		}
		
		return returnValue;
	}
	
	public List<Book> loadBooks(){
		List<Book> returnValue = new ArrayList<>();
		try {
			returnValue = this.BookDAO.loadBooks();
		} catch (HibernateException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
		}
		return returnValue;
	}

	public boolean deleteBook(Book b) {
		boolean returnValue = false;
		try {
			returnValue = this.BookDAO.deleteBook(b);
		} catch (HibernateException e) {
			LOGGER.log(Level.INFO, e.getMessage(), e);
		}
		return returnValue;
	}
	
}
