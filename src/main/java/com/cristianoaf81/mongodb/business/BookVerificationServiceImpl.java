package com.cristianoaf81.mongodb.business;

import com.cristianoaf81.mongodb.business.exceptions
.BookDataVerificationException;

import com.cristianoaf81.mongodb.model.Book;

public class BookVerificationServiceImpl implements BookVerificationService {

	@Override
	public boolean verifyBookData(Book b) 
			throws BookDataVerificationException {
		
		boolean returnValue = false;
		
		if(b.getAuthor().isEmpty() || b.getAuthor() == null 
				||	b.getTitle().isEmpty() || b.getTitle() == null 
				||	b.getType().isEmpty() || b.getType() == null 
				|| 	b.getPrice() <= 0D || b.getCopies() <= 0) 
		{
			
			throw new 
			BookDataVerificationException("The book data cannot be empty!");
			
		}else {
			
			returnValue = true;
			
		}
		
		
		return returnValue;
	}

}
