package com.cristianoaf81.mongodb.business;
import com.cristianoaf81.mongodb.business.exceptions.*;
import com.cristianoaf81.mongodb.model.Book;

public interface BookVerificationService {
	
	boolean verifyBookData(Book b) 
			throws BookDataVerificationException;
	
}
