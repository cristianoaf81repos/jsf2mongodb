package com.cristianoaf81.mongodb.api.entrypoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.BeanUtils;

import com.cristianoaf81.mongodb.api.requests.BookDTO;
import com.cristianoaf81.mongodb.business.BookBO;
import com.cristianoaf81.mongodb.business.BookVerificationService;
import com.cristianoaf81.mongodb.business.BookVerificationServiceImpl;
import com.cristianoaf81.mongodb.business.exceptions.BookDataVerificationException;
import com.cristianoaf81.mongodb.model.Book;


@SuppressWarnings("unused")
@Path("/books")
public class BooksEntryPoint {
	
	List<Book> books;
	private Logger LOGGER;
	private BookBO BusinessOperation;
	private BookVerificationService bookVerification;
	
	
	public BooksEntryPoint() {
		books = new ArrayList<Book>();
		LOGGER = Logger.getLogger(BooksEntryPoint.class.getName());
		BusinessOperation = new BookBO();
		bookVerification = new BookVerificationServiceImpl();
		try {
			books =  BusinessOperation.loadBooks();
		} catch (HibernateException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	@Produces({MediaType.APPLICATION_JSON})
	@GET
	public Response getAllBooks(){
		
		try {
			if(!books.isEmpty() || books.size()>0) {
				// converte em entidade generica
				GenericEntity<List<Book>> genericEntity = 
						new GenericEntity<List<Book>>(books) {};
				
				return Response.status(Status.OK).entity(genericEntity).build();
			}
			
		}catch (HibernateException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);			
		}
						
		return Response.status(Status.OK)
				.entity("Nenhum livro cadastrado!")
				.build();
	}
	
	
	@GET
	@Path("/{BookType}")
	@Produces({MediaType.APPLICATION_JSON})	
	public Response GetBooksByType(@PathParam("BookType") String BookType) {
		try {
			
			if(books.size() > 0 || !books.isEmpty()) {
				
				List<Book> BooksByType = new ArrayList<Book>();
							
				books.forEach(b->{
					if(StringUtils.stripAccents(b.getType().toLowerCase().trim())
							.contains(BookType.toLowerCase().trim())) {
						BooksByType.add(b);
					}					
				});
				
				GenericEntity<List<Book>> genericEntity = 
						new GenericEntity<List<Book>>(BooksByType) {};
						
							
				return Response.ok().entity(genericEntity).build();
			}
			
		} catch (HibernateException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		
		return Response.status(Status.OK)
				.entity("Não-Encontrado").build();
	}
	
	@POST
	@Path("/newbook")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newBook(BookDTO bookRequest) {
		Book book = new Book();
		BeanUtils.copyProperties(bookRequest, book);
		Map<String,Object> responseMap = new HashMap<String, Object>();
		try {
			boolean isBookDataOk = this.bookVerification.verifyBookData(book);
			if(isBookDataOk) {
				book.setId("");
				boolean isSaved = BusinessOperation.saveBook(book);
				if(isSaved) {
					responseMap.put("operation_status", "save sucessfull!!!");
					responseMap.put("Book_object_data",book);
					return Response.status(Status.OK)
							.entity(responseMap).build();
				}else {
					responseMap.put("operation_status", "save failed!!!");
					responseMap.put("Book_object_data",book);
					return Response.status(Status.OK)
							.entity(responseMap).build();
				}				
			}else {
				responseMap.put("operation_status", "error book data is incomplete or fields name's wrong!");
				responseMap.put("Book_object_data",book);
			}
		} catch (BookDataVerificationException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		
		return Response.status(400).entity(responseMap).build();
		//return Response.ok().entity(book.getAuthor()).build();
	}
	
}
