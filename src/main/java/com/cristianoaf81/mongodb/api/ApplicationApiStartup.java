package com.cristianoaf81.mongodb.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class ApplicationApiStartup extends Application{

}
