package com.cristianoaf81.mongodb.api.requests;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement
@Data
public class BookDTO {
	String title;
	String author;
	String type;
	double price;
	int copies;	
}
