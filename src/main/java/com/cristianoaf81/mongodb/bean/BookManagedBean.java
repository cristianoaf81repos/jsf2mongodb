package com.cristianoaf81.mongodb.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.cristianoaf81.mongodb.business.BookBO;
import com.cristianoaf81.mongodb.model.Book;

@SuppressWarnings("unused")
@ManagedBean(name = "BookMB")
@SessionScoped
public class BookManagedBean {
	
	// para implementar
	/* 
	 * 
	 * criar metodo para limpar tela ok
	 * criar lista de livros ok
	 * metodo para atualizar livro da lista
	 * metodo para excluir livro da lista
	 * criar metodo para buscar livros da lista
	 * criar objeto book para a view
	 * 
	 * */
	
		
	private Book book = new Book();
	private BookBO BusinessOperation = new BookBO();
	private Logger LOGGER = Logger.getLogger(BookManagedBean.class.getName());
	private List<Book> books;
	
	
	
	public BookManagedBean() {
		books = BusinessOperation.loadBooks();		
	}
			
	
	public List<Book> getBooks() {
		return this.books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;		
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public void saveBook(Book b) {
		
		boolean isUpdate = !b.getId().equals("") 
				|| b.getId() != null ? true:false;
		
		boolean isSaved = this.BusinessOperation.saveBook(b);
		
		FacesMessage message = new FacesMessage();
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if(isSaved) {
			
			message.setDetail("Situação da operação:");
			
			message.setSummary("Livro Salvo com sucesso!");
			
			message.setSeverity(FacesMessage.SEVERITY_INFO);
			
			// adiciona o livro ao array list sem fazer o reload do bd
			// mas se houver update recarrega a lista
			if(!isUpdate)
				this.books.add(b);
			else {
				this.books.clear();
				this.books = BusinessOperation.loadBooks();
				book = new Book();
			}
			
			// limpa a tela
			book = new Book();
			
			context.addMessage(null, message);
			
		}else {
			message.setDetail("Situação da operação:");
			
			message.setSummary(
					"Falha ao salvar dados favor verifique o log do sistema");
			
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			
			context.addMessage(null, message);
		}
	}

	public void clearFormData() {
		
		this.book = new Book();
		
		try {
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			ExternalContext externalContext = context.getExternalContext();
			
			externalContext.redirect("index.jsf");
			
		}catch(IOException ioe) {
			
			LOGGER.log(Level.SEVERE, ioe.getMessage(), ioe);
			
		}
	}
	
		
	public String bookEdit() {
		
		return "index.jsf";
		
	}
	
	public void deleteBook(Book b) {
		
		if(b != null && !b.getId().equals("")) {
			
			FacesMessage message = new FacesMessage();
			
			FacesContext context = FacesContext.getCurrentInstance();
			
			try {
				
				boolean isDeleted = BusinessOperation.deleteBook(b);
				
				if(isDeleted) {
					
					this.books.remove(b);
					
					message.setDetail("Situação da operação:");
					
					message.setSummary("Livro removido com sucesso!");
					
					message.setSeverity(FacesMessage.SEVERITY_INFO);
					
					context.addMessage(null, message);
					
				}else {
					message.setDetail("Situação da operação:");
					
					message.setSummary("Falha ao remover livro favor"
							+ " verifique o log do sistema");
					
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					
					context.addMessage(null, message);
				}
					
			}catch(Exception e) {
				
				LOGGER.log(Level.SEVERE, e.getMessage(), e);
				
			}			
		}
		
	}
	
}
